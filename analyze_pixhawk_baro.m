% clear
clear variables; clc; close all;

%% read data
[num, txt, raw] = xlsread('pixhawk_baro_1.xlsx');
Ts              = 0.01;
Fs              = 1/Ts; 

h_baro_selected = num(1:30000); 
t = ( 0:length(h_baro_selected)-1 ) * Ts; 

%% remove mean
mu              = mean(h_baro_selected); 
h_baro_selected = h_baro_selected - mu; 
M               = length(h_baro_selected); 

figure;
plot(t, h_baro_selected); 

%% compute auto correlation: Rx
[h_baro_ac, lags]   = xcorr(h_baro_selected, 'biased');   % 'biased'- scales the raw cross-correlation by 1/M

% display the correlation results
hAC     = figure(); 
plot(lags(M:2*M-1)*Ts, h_baro_ac(M:2*M-1)); % only plot positive side of autocorrelation
grid on; hold on; ylabel('Auto Correlation'); xlabel('time lag \tau (s)'); 

%% computer the power spectral density
sel_psd = 1; % 1: using Welch spectral estimator; 2: using FFT

%% Welch's method 
if ( sel_psd == 1 )
    sp_welch    = spectrum.welch;                           % Create a Welch spectral estimator. 
    h_baro_psd0 = psd(sp_welch, h_baro_selected, 'Fs', Fs); % Calculate the PSD 

    % Plot the PSD
    hPSD       = figure(); 
    plot(h_baro_psd0); hold on;
    
    h_baro_psd  = h_baro_psd0.data; 
    
    % parameter estimation
    xdata = h_baro_psd0.frequencies; 
    ydata = h_baro_psd0.data; 
    
    x = lsqcurvefit(@(x,xdata) x(3)^2*( 1+x(1)^2*(2*pi*xdata).^2 )./ ( 1+x(2)^2*(2*pi*xdata).^2 ) ,[0.01, 0.1, 0.7], xdata(1:33), ydata(1:33) );
    tau_o       = abs( x(1) );
    tau_p       = abs( x(2) );
    N_o         = abs( x(3) ); 

%     x = lsqcurvefit(@(x,xdata) ydata(1)*( 1 + x(1)^2*(2*pi*xdata).^2 )./ ( 1+x(2)^2*(2*pi*xdata).^2 ) ,[0.01 0.1], xdata(1:17), ydata(1:17) );
%     tau_o       = abs(x(1)) ;
%     tau_p       = abs(x(2)) ;
%     N_o         = sqrt( ydata(1) ); 
    
    alpha       = tau_o / tau_p * N_o;
    beta        = sqrt( N_o^2 - alpha^2 );  

    % std of correlated noise
    sigma_c     = beta / sqrt( 2*tau_p );
    
    % time constant of correlated noise
    tau         = tau_p;
    
    % std of uncorrelated noise
    sigma_u      = alpha; 
    
    % simulation
    freq        = xdata; 
    Sx_sim      = ( alpha^2 + beta^2 ) .* (1 + 2*pi*2*pi*freq.*freq * tau_o * tau_o ) ./ (1 + 2*pi*2*pi*freq.*freq * tau_p * tau_p );  
    
    figure(hPSD);
    plot(freq, 10*log10(Sx_sim), 'r'); grid on; axis tight; 
    legend('measured', 'estimated');
    title('Power Spectral Density'); 

    display(tau);
    display(sigma_c);
    display(sigma_u);
end

if ( sel_psd == 2 )
    % obtain nonparametric power spectral density (PSD) estimates
    % equivalent to the periodogram using fft
    N               = length(h_baro_selected);
    xdft            = fft(h_baro_selected);
    xdft            = xdft(1:N/2+1);
    psdx            = (1/(Fs*N)) * abs(xdft).^2;
    psdx(2:end-1)   = 2*psdx(2:end-1);
    freq            = 0:Fs/length(h_baro_selected):Fs/2;

    hPSD = figure();
    plot(freq,10*log10(psdx)); grid on; hold on; 
    title('Periodogram Using FFT')
    xlabel('Frequency (Hz)')
    ylabel('Power/Frequency (dB/Hz)'); 
    
    h_baro_psd      = psdx; 
    
end

if ( sel_psd == 3 )
    
    [pxx,f] = pwelch(h_baro_selected,500,300,500,Fs);

    plot(f,10*log10(pxx))
    xlabel('Frequency (Hz)')
    ylabel('Magnitude (dB)')
    grid
end

if ( sel_psd == 4 )
    [Pxx,F] = periodogram(h_baro_selected,[],length(h_baro_selected),Fs);
    figure; 
    plot(F,10*log10(Pxx))
end



%% %%%%%%%%%%%%%%%%  Backup codes   %%%%%%%%%%%%%%%%%%%%%%%%%

% %% compute the parameters
% sel_method = 2; % 1: compute using autocorrelation and PSD; 2: estimation using ARMA model
% 
% if ( sel_method == 1 ) % simple calculation using Rx(0) and Sx(0)
%     
%     % compute sigma_c, standard diviation
%     Rx_0        = h_baro_ac(index_end-index_start+1); 
%     sigma_c     = sqrt( Rx_0 ); 
%     
%     % compute time constant: tau
%     Sx_0        = max(h_baro_psd); 
%     tau         = Sx_0 / ( 2 * sigma_c^2 );
%     
%     % simulate Rx and Sx
%     t_lags      = lags(M:2*M-1)/Fs; 
%     Rx_sim      = sigma_c^2 * exp( - 1 / tau .* t_lags); 
%     
%     freq = 0:Fs/length(h_baro_selected):Fs/2;
%     Sx_sim      = 2 * sigma_c^2 * 1 / tau ./ ( 2*pi*2*pi*freq.*freq + 1 / tau^2 );  
% end
% 
% 
% 
% if ( sel_method == 2 ) % estimation using ARMA model
%     
%     %% Estimate ARMA model parameters
%     %
%     %  Error model of barometric sensor can be represented by an ARMA(1,1) 
%     %  n(t) = nu(t) + n_c(t) + n_u(t)
%     %  
%     %  nu is the trend and ignored here
%     %  n_c(t) is the correlated noise
%     %  n_u(t) is the 
% 
%     past_data   = iddata(h_baro_selected,[], Ts); 
% 
%     sys         =  armax(past_data, [1, 1]);
% 
%     tau_p       = 0.5 / Fs *( 1 - sys.a(2) ) / ( 1 + sys.a(2) );
%     tau_o       = 0.5 / Fs *( 1 - sys.c(2) ) / ( 1 + sys.c(2) );
% 
%     % alpha_u   = sqrt( sys.NoiseVariance ) ; 
%     % 
%     % beta_c    = sqrt( ( alpha_u/tau_o )^2 - alpha_u^2 ); 
%     % 
%     % alpha_c   = beta_c / sqrt( 2 * tau_p );
% 
%     H_o         = sqrt( sys.NoiseVariance ) ; 
% 
%     alpha       = tau_o / tau_p * H_o * ( 1 + 2 * tau_p * Fs ) / ( 1 + 2 * tau_o * Fs  );
% 
%     beta        = alpha * sqrt( ( tau_p / tau_o )^2-1 );  
% 
%     % std of correlated noise
%     sigma_c     = beta / sqrt( 2*tau_p );
%     
%     % time constant of correlated noise
%     tau         = tau_p;
%     
%     % std of uncorrelated noise
%     sigma_u      = alpha; 
%     
%     %% simulate Rx and Sx
%     
%     %sigma_square = alpha_c^2; 
%     %beta         = 1 / tau_p;  
% 
%     t_lags      = lags(M:2*M-1)/Fs; 
%     Rx_sim      = sigma_c^2 * exp(- ( 1 / tau ) .* t_lags); 
%     
%     freq = 0:Fs/length(h_baro_selected):Fs/2;
%     %Sx_sim     = ( alpha_u^2 + beta_c^2 ) .* (1 + 2*pi*2*pi*freq.*freq * tau_o * tau_o ) ./ (1 + 2*pi*2*pi*freq.*freq * tau_p * tau_p );  
%     Sx_sim      = ( alpha^2 + beta^2 ) .* (1 + 2*pi*2*pi*freq.*freq * tau_o * tau_o ) ./ (1 + 2*pi*2*pi*freq.*freq * tau_p * tau_p );  
% 
%     
%     %% Perform a 10-step ahead prediction to validate the model over the  
%     % time-span of the measured data.
%     yp = predict(sys, h_baro_selected,10);
% 
%     %figure(hBaroSelected); 
% %     figure;
% %     plot(t_selected, yp, 'r*');
% %     legend('Past Data','Predicted Data');
%     
%     % frequency response of the overal shaping filter
%     figure;
%     freqz(sys.c*sqrt( sys.NoiseVariance ) , sys.a  );
%     [h, w] = freqz(sys.c*H_o , sys.a  );
%     %     figure(h_psd);
%     %     plot(w/pi*Fs/2, 20*log10(abs(h)), 'c--'); legend('measured', 'simulated');
% end





