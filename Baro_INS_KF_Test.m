%% clear the workspace
clear variables; close all; clc;
clear h_KF_baro_ins_4_state;


%%  -------------------  Load data   ---------------------------------
sel_file        = 11; 

if ( sel_file<=10 )  % 10 Hz data
    
    if ( sel_file == 1)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav1.csv');
        index_start     = 2000;      % start index of the selected data
        index_end       = 5000;      % end index of the selected data
    end 

    if ( sel_file == 2)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav2.csv');
        index_start     = 100;     
        index_end       = 1100;     
    end 

    if ( sel_file == 3)  
        [num, txt, raw] = xlsread('result01_2017_01_19_uav3.csv');
        index_start     = 1;     
        index_end       = 3000;  
    end 

    if ( sel_file == 4)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav4.csv');
        index_start     = 1;     
        index_end       = 3000;     
    end 

    if ( sel_file == 5)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav5.csv');
        index_start     = 100;      
        index_end       = 700;     
    end 

    data            = num(3:end, :);

    %% run time
    t               = ( data(:,340) - data(1,340) ) * 0.000001; % time
    Fs              = 10;                   % sampling frequency
    Ts              = 1/Fs;                 % sampling time

    %% attitude measurement
    phi             = data(:,1);    % roll angle
    tht             = data(:,2);    % pitch angle
    psi             = data(:,3);    % yaw angle

    %% imu raw data
    acx             = data(:,14);   % imu x acceleration
    acy             = data(:,15);   % imu y acceleration
    acz             = data(:,16);   % imu z acceleartion

    %% baro data
    p_baro          = data(:,41);                 % baro pressure
    h_baro          = data(:,42) - data(1,42);    % baro height, remove bias
    d_baro          = data(:,43);                 % baro temperature in degree 

    h_laser         =-data(:,49);   % laser range finder


    %% Local position. LPOS_XYZ etc.
    x               = data(:,46);   % N position
    y               = data(:,47);   % E position
    z               = data(:,48);   % D position

    u               = data(:,51);   % N velocity 
    v               = data(:,52);   % E velocity 
    w               = data(:,53);   % D velocity 

else % 100 Hz for Pixhwck with new firmware
    
    if ( sel_file == 11)
        [num, txt, raw] = xlsread('pixhawk_baro_1_covered_reduced.csv');   
        index_start     = 1;      
        index_end       = 60000;  
    end 
    
    data0           = num(3:end, :);
    data            = data0(index_start:index_end, :); 
    
    %% run time
    t               = ( data(:,18) - data(1,18) ) * 0.000001; % time
    Fs              = 100;                  % sampling frequency
    Ts              = 1/Fs;                 % sampling time

    %% attitude measurement
    phi             = data(:,1);    % roll angle
    tht             = data(:,2);    % pitch angle
    psi             = data(:,3);    % yaw angle

    %% imu raw data
    acx             = data(:,4);    % imu x acceleration
    acy             = data(:,5);    % imu y acceleration
    acz             = data(:,6);    % imu z acceleartion

    %% baro data
    p_baro          = data(:,7);                 % baro pressure
    h_baro          = data(:,8) - data(1,8);     % baro height, remove bias
    d_baro          = data(:,9);                 % baro temperature in degree 

    h_laser         =-data(:,14);   % laser range finder


    %% Local position. LPOS_XYZ etc.
    x               = data(:,10);   % N position
    y               = data(:,11);   % E position
    z               = data(:,12);   % D position

    u               = data(:,15);   % N velocity 
    v               = data(:,16);   % E velocity 
    w               = data(:,17);   % D velocity 
end
 
%% acceleration in NED frame
agg = zeros(length(t),3); 
for i=1:length(t)
    % rotation matrix
    R           = R_Body2NED(phi(i), tht(i), psi(i));    
    
    % convert acceleration to NED frame
    agg(i,:)    = (R * [acx(i); acy(i); acz(i)])'; 
end


%% body acceleration
figure;
subplot(3,1,1); 
plot(t, acx, 'r'); ylabel('acx (m/s^2)'); grid on; axis tight; title('body acceleration'); 
subplot(3,1,2); 
plot(t, acy, 'r'); ylabel('acy (m/s^2)'); grid on; axis tight; 
subplot(3,1,3); 
plot(t, acz, 'r'); ylabel('acz (m/s^2)'); grid on; axis tight; 


%% baro calibration
P   = polyfit(d_baro, -h_baro, 3);  
f   = polyval(P,d_baro);

%% disply baro infor
hBaroInfor = figure();
subplot(2,1,1);
%plot(t, -h_baro, 'g-.', t, z, t, h_laser, 'c-.', 'linewidth', 2); hold on;
plot(t, -h_baro, 'g-.', t, z, t, f, 'k--', 'linewidth', 2); hold on; 
axis tight; grid on; ylabel('z (m)'); xlabel('time (s)'); 
legend('Baro raw', 'PX4 EKF', 'Baro calib poly'); 

subplot(2,1,2);
plot(t, d_baro, 'linewidth', 2); 
axis tight; grid on; ylabel('temperature (degree)'); xlabel('time (s)'); 

% correct the baro
h_baro  = - ( -h_baro - f ); 

%% ************************************************************************
%  Kalman filtering 
%
sel_KF = 1; % 1: 4-state KF with baro's correlated error; 2: 3-state KF without baro's correlated error

% initialization
if ( sel_KF == 1 ) 
    P_kf    = diag([10 2 0.1 1]); 
    x_kf    = [x(1); 0; 0; 0];
    kfout   = zeros(length(t), 4);
end

if ( sel_KF == 2 ) 
    P_kf    = diag([10 2 0.1]); 
    x_kf    = [x(1); 0; 0];
    kfout   = zeros(length(t), 3);
end

for i = 1:length(t)    

    % inputs of Kalman filter
    u_kf = agg(i,:)'+ [0; 0; 9.781]; 
    
    u_thr = 3; % the constrain of the acceleration input
    for j=1:3
        if u_kf(j) < - u_thr
            u_kf(j) = -u_thr;
        end

        if u_kf(j) > u_thr 
            u_kf(j) = u_thr; 
        end
    end
    
    % run Kalman filtering for altitude estimation
    if ( sel_KF == 1 ) 
        [x_kf, K_kf, P_kf] = h_KF_baro_ins_4_state(x_kf, u_kf(3), -h_baro(i), P_kf, Ts);  
    end 
    
    if ( sel_KF == 2 ) 
        [x_kf, K_kf, P_kf] = h_KF_baro_ins_3_state(x_kf, u_kf(3), -h_baro(i), P_kf, Ts);  
    end 
    
    kfout(i,:) = x_kf';
    
end

%% ************************************************************************
% plot the filtering results
figure;
subplot(2,1,1);
plot(t, -h_baro, 'g--', t, z, 'b-', t, kfout(:,1), 'r-', 'linewidth', 2); 
axis tight; grid on; ylabel('z (m)'); xlabel('time (s)'); 
legend('Baro raw', 'Px4 EKF', 'Proposed KF'); 

subplot(2,1,2);
plot(t, w, t, kfout(:,2), 'r-','linewidth', 2); 
axis tight; grid on; ylabel('w (m/s)'); 
legend('Baro raw', 'Proposed KF'); 

if ( sel_KF == 1 ) 
    figure;
    subplot(2,1,1);
    plot(t, kfout(:,3), 'r-', 'linewidth', 2); axis tight; grid on; 
    ylabel('az_{bias} (m/S^2)'); 

    subplot(2,1,2);
    plot(t, kfout(:,4), 'r-', 'linewidth', 2); axis tight; grid on; 
    ylabel('e_{baro} (m)'); 
end 

if ( sel_KF == 2 ) 
    figure;
    plot(t, kfout(:,3), 'r-', 'linewidth', 2); axis tight; grid on; 
    ylabel('az_{bias} (m/S^2)'); 
end 