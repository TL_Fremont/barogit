%% Load data
clear all; close all;
clc;

%data = csvread('result01.csv', 2, 0);

[num, txt, raw] = xlsread('result01_2017_01_19_uav1.csv');
data = num(3:end, :);

%% run time
t         = data(:,340)*0.000001; % time
freq      = 50;
startTimeIndex = 1; 
t_min     = t(startTimeIndex);
t_max     = max(t);
ind_plot  = floor(t_min*freq):size(t,1);

%% attitude measurement
phi = data(:,1); % roll angle
tht = data(:,2); % pitch angle
psi = data(:,3); % yaw angle

p   = data(:,4); % roll anglular rate
q   = data(:,5); % pitch anglular rate
r   = data(:,6); % yaw anglular rate

phi_r = data(:,10); % roll reference
tht_r = data(:,11); % pitch reference
psi_r = data(:,12); % yaw reference
thr_r = data(:,13); % thrust reference

%% imu raw data
acx = data(:,14); % imu x acceleration
acy = data(:,15); % imu y acceleration
acz = data(:,16); % imu z acceleartion

p_raw = data(:,17); % roll rate raw
q_raw = data(:,18); % pitch rate raw
r_raw = data(:,19); % yaw rate raw

mag_x = data(:,20); % magnetometer x
mag_y = data(:,21); % magnetometer y
mag_z = data(:,22); % magnetometer z

%% baro data
p_baro = data(:,41); % baro pressure
h_baro = data(:,42);% baro height
h_baro = - ( h_baro - h_baro(1) ); 

temp_baro =  data(:,43); % baro temperature

%% Local position. LPOS_XYZ etc.
x = data(:,46); % N position
y = data(:,47); % E position
z = data(:,48); % D position

z_range_finder = - data(:,49);
diff_range_kf = data(:,49) + data(:,48);

u = data(:,51); % N velocity 
v = data(:,52); % E velocity 
w = data(:,53); % D velocity 

% local position set point
x_r = data(:,62); % N position reference
y_r = data(:,63); % E position reference
z_r = data(:,64); % D position reference

% imav nuc reference
ref_x=data(:,62);
ref_y=data(:,63);
ref_z=data(:,64);
ref_yaw=data(:,336);

%% GPS coonfiguration data
fix = data(:,76); % GPS fix
eph = data(:,77); % GPS EPH
epv = data(:,78); % GPS EPV

lat = data(:,79); % GPS latitude
lon = data(:,80); % GPS longitude
alt = data(:,81); % GPS altitude

v_N = data(:,83); % GPS latitude
v_E = data(:,84); % GPS longitude
v_D = data(:,85); % GPS altitude

%% attitude control
ail = data(:,81); % aileron
ele = data(:,82); % elevator
rud = data(:,83); % rudder
thr = data(:,84); % throttle

%% flight state
stat_main = data(:,85); % main state
stat_arm  = data(:,86); % arming state
stat_fail = data(:,87); % failsafe state
stat_batr = data(:,88); % battery rem state
stat_batw = data(:,89); % battery warn state
stat_land = data(:,90); % landed state
stat_nav  = data(:,91); % navigation state

%% control signal
RC1 = data(:,92); % Aileron
RC2 = data(:,93); % Elevator
RC3 = data(:,94); % Throttle
RC4 = data(:,95); % Rudder
RC5 = data(:,96); % Main mode switch
RC6 = data(:,97); % Loiter switch
RC7 = data(:,98); % Return home switch

%% global velosity setpoint
u_r = data(:,153); % N velocity reference
v_r = data(:,154); % E velocity reference
w_r = data(:,155); % D velocity reference

%%
out1 = data(:,102);
out2 = data(:,103);
out3 = data(:,104);
out4 = data(:,105);
out5 = data(:,106);
out6 = data(:,107);
out7 = data(:,108);
out8 = data(:,109);

thr_sp = (out1+out2+out3+out4)/4;

rate_sp_roll = data(:,113);
rate_sp_pitch = data(:,114);
rate_sp_yaw = data(:,115);

triggerFlags = data(:,329);

%% GPS lat measurement
% figure
% subplot(211); plot(t, lat,'r'); xlim([t_min t_max]); legend('lat'); title('Latitude')
% subplot(212); 
% plot(t,x,'.-b', t, u, 'g', t, v_N, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('x','u','v-N')
% title('x & u & v-N')


% figure
% plot(lat(startTimeIndex:end),lon(startTimeIndex:end)); 

%% 2D trajectory
figure
plot(x(startTimeIndex:end),y(startTimeIndex:end)); title('2D trajectory'); 
xlabel('x'); ylabel('y');
%% GPS lon measurement
% figure
% subplot(211); plot(t, lon,'r'); xlim([t_min t_max]); legend('lon');title('Longitude');xlabel('time (s)');
% subplot(212); 
% plot(t,y,'.-b', t, v, 'g', t, v_E, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('y','v','v-E');xlabel('time (s)'); 
% title('y & v & v-E')

%% GPS height measurement
 figure
 subplot(211); plot(t, alt, 'r',t,z, '.-b', t, h_baro,'g', t, z_range_finder, 'k', t, diff_range_kf, 'c'); xlim([t_min t_max]); title('Z-axis GPS alt, z, h-baro, rangeFinder, diff_range_kf');xlabel('time (s)'); 
 legend('alt','z', 'h-baro', 'rangeFinder', 'diff_range_kf')
% % subplot(312); plot(t, z, 'LineWidth',1.5);  xlim([t_min t_max]);
% % legend('z');xlabel('time (s)');  title('Z-axis pos estimate');
% subplot(212); plot(t, w, 'g', t, v_D, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('w','v-D');title('Z-axis vel estimate');xlabel('time (s)');

%% Acceleration Plot
% figure; 
% subplot(3,1,1); plot(t,acx); grid on; title('x-axis'); xlim([t_min t_max]);legend('acx'); title('X Acceleration')
% subplot(3,1,2); plot(t,acy); grid on; title('y-axis'); xlim([t_min t_max]);legend('acy'); title('Y Acceleration')
% subplot(3,1,3); plot(t,acz); grid on; title('z-axis'); xlim([t_min t_max]);legend('acz'); title('Z Acceleration')

%% X Y Z H : Reference tracking for control
figure; 
subplot(2,2,1); plot(t,x,'.b',t,ref_x,'r', t,u,'.g',t,u_r,'k'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('x-axis','FontSize',14); legend('x','x-ref','ug','ug-ref'); xlabel('time (s)'); ylabel('x-axis p & v (m & m/s)');

subplot(2,2,2); plot(t,y,'.b',t,ref_y,'r', t,v,'.g',t,v_r,'k'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('y-axis','FontSize',14); legend('y','y-ref','vg','vg-ref'); xlabel('time (s)'); ylabel('y-axis p & v (m & m/s)');

subplot(2,2,3); plot(t,z,'.b',t, ref_z,'r');  xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('z-axis','FontSize',14); legend('z','z-ref','wg','wg-ref'); xlabel('time (s)'); ylabel('z-axis p & v (m & m/s)');
%, t, w,'.g', t, w_r,'k'
subplot(2,2,4); plot(t,psi*180/pi,'.b',t,psi_r*180/pi, 'r'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('Heading','FontSize',14); legend('\psi','\psi-ref'); xlabel('time (s)'); ylabel('Yaw angle (deg)'); 
%% %% Attitude control performance
% figure;
% subplot(3,2,1); plot(t, phi_r, 'r', t, phi, 'b');   xlim([t_min t_max]); ylabel('\phi (rad)'); grid on;
% subplot(3,2,3); plot(t, p, 'b',t,rate_sp_roll,'r'); xlim([t_min t_max]); ylabel('p (rad/s)'); grid on;
% subplot(3,2,5); plot(t, ail, 'b'); xlim([t_min t_max]); ylabel('\delta_{ail} [-1 1]'); grid on;
% 
% subplot(3,2,2); plot(t, tht_r, 'r', t, tht, 'b'); xlim([t_min t_max]); ylabel('\theta (rad)'); grid on;
% subplot(3,2,4); plot(t, q, 'b',t,rate_sp_pitch,'r'); xlim([t_min t_max]); ylabel('q (rad/s)'); grid on;
% subplot(3,2,6); plot(t, ele, 'b'); xlim([t_min t_max]); ylabel('\delta_{ele} [-1 1]'); grid on;

%% Mode switches
% figure;
% plot(t, RC5, 'b', t, RC6, 'r', t, RC7, 'k'); ylim([-1.5 1.5]); legend('Mode switch', 'Loiter switch', 'Return home'); xlabel('time (s)'); grid on;

%% RC plot
% figure; 
% subplot(4,2,1); plot(t, RC1); ylabel('RC1'); grid on;
% subplot(4,2,3); plot(t, RC2); ylabel('RC2'); grid on;
% subplot(4,2,5); plot(t, RC3); ylabel('RC3'); grid on;
% subplot(4,2,7); plot(t, RC4); ylabel('RC4'); grid on;
% 
% subplot(4,2,2); plot(t, RC5); ylabel('RC5'); grid on;
% subplot(4,2,4); plot(t, RC6); ylabel('RC6'); grid on;
% subplot(4,2,6); plot(t, RC7); ylabel('RC7'); grid on;
% 
% %%
% figure; 
% subplot(5,1,1); plot(t,stat_main); ylabel('stat\_main');
% subplot(5,1,2); plot(t,stat_nav); ylabel('stat\_nav');
% subplot(5,1,3); plot(t,stat_land); ylabel('stat\_land');
% subplot(5,1,4); plot(t,stat_arm); ylabel('stat\_arm');
% subplot(5,1,5); plot(t,triggerFlags); ylabel('triggerFlags');
%% output plot
% figure; 
% hold on
% plot(t,out1,'r');ylabel('Out1'); grid on;
% plot(t,out2,'g');ylabel('Out2'); grid on;
% plot(t,out3,'b');ylabel('Out3'); grid on;
% plot(t,out4,'k');ylabel('Out4'); grid on;
% title('RC PWM output')
% legend('Out1','Out2','Out3','Out4');

% % plot(t,out5,'g.');ylabel('Out5');
% % plot(t,out6,'b.');ylabel('Out6');
% % plot(t,out7,'y.');ylabel('Out7');
% % plot(t,out8,'y');ylabel('Out8');
% legend('Out1','Out2','Out3','Out4','Out5','Out6','out7','out8');
% 
% p_dot = zeros(size(t));
% for i = 1:size(t,1)-1
%    p_dot(i) =  (p(i+1)-p(i))/(t(i+1)-t(i));  
% end

