clear variables; clc; close all;


%%  -------------------  Load data   ---------------------------------
sel_file        = 5; 

if ( sel_file<=10 )  % 10 Hz data
    
    if ( sel_file == 1)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav1.csv');
        index_start     = 2000;     % in flight data
        index_end       = 5000;      
    end 

    if ( sel_file == 2)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav2.csv');
        index_start     = 2000;     % in flight data
        index_end       = 5000;     
    end 

    if ( sel_file == 3)  
        [num, txt, raw] = xlsread('result01_2017_01_19_uav3.csv');
        index_start     = 8000;     % in flight data
        index_end       = 10000;  
    end 

    if ( sel_file == 4)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav4.csv');
        index_start     = 8000;     % in flight data 
        index_end       = 10000;     
    end 

    if ( sel_file == 5)
        [num, txt, raw] = xlsread('result01_2017_01_19_uav5.csv');
        index_start     = 1500;     % in flight data  
        index_end       = 4500;     
    end 

    %data            = num(3:end, :);
    
    data            = num(index_start:index_end, :);
    
    %% run time
    t               = ( data(:,340) - data(1,340) ) * 0.000001; % time
    Fs              = 10;                   % sampling frequency
    Ts              = 1/Fs;                 % sampling time

    %% attitude measurement
    phi             = data(:,1);    % roll angle
    tht             = data(:,2);    % pitch angle
    psi             = data(:,3);    % yaw angle

    %% imu raw data
    acx             = data(:,14);   % imu x acceleration
    acy             = data(:,15);   % imu y acceleration
    acz             = data(:,16);   % imu z acceleartion

    %% baro data
    p_baro          = data(:,41);                 % baro pressure
    h_baro          = data(:,42) - data(1,42);    % baro height, remove bias
    d_baro          = data(:,43);                 % baro temperature in degree 

    h_laser         =-data(:,49);   % laser range finder


    %% Local position. LPOS_XYZ etc.
    x               = data(:,46);   % N position
    y               = data(:,47);   % E position
    z               = data(:,48);   % D position

    u               = data(:,51);   % N velocity 
    v               = data(:,52);   % E velocity 
    w               = data(:,53);   % D velocity 

else % 100 Hz for Pixhwck with new firmware
    
    if ( sel_file == 11)
        [num, txt, raw] = xlsread('pixhawk_baro_1_covered_reduced.csv');   
        index_start     = 5000;      ground data
        index_end       = 10000;  
    end 
    
    data0           = num(3:end, :);
    data            = data0(index_start:index_end, :); 
    
    %% run time
    t               = ( data(:,18) - data(1,18) ) * 0.000001; % time
    Fs              = 100;                  % sampling frequency
    Ts              = 1/Fs;                 % sampling time

    %% attitude measurement
    phi             = data(:,1);    % roll angle
    tht             = data(:,2);    % pitch angle
    psi             = data(:,3);    % yaw angle

    %% imu raw data
    acx             = data(:,4);    % imu x acceleration
    acy             = data(:,5);    % imu y acceleration
    acz             = data(:,6);    % imu z acceleartion

    %% baro data
    p_baro          = data(:,7);                 % baro pressure
    h_baro          = data(:,8) - data(1,8);     % baro height, remove bias
    d_baro          = data(:,9);                 % baro temperature in degree 

    h_laser         =-data(:,14);   % laser range finder


    %% Local position. LPOS_XYZ etc.
    x               = data(:,10);   % N position
    y               = data(:,11);   % E position
    z               = data(:,12);   % D position

    u               = data(:,15);   % N velocity 
    v               = data(:,16);   % E velocity 
    w               = data(:,17);   % D velocity 
end
 
%% acceleration in NED frame
agg = zeros(length(t),3); 
for i=1:length(t)
    % rotation matrix
    R           = R_Body2NED(phi(i), tht(i), psi(i));    
    
    % convert acceleration to NED frame
    agg(i,:)    = (R * [acx(i); acy(i); acz(i)])'; 
end

% anz_selected    = num(1:30000); 
% t = ( 0:length(h_baro_selected)-1 ) * Ts; 

anz_selected    = agg(:,3) + 9.781;

% remove mean
mu              = mean(anz_selected); 
anz_selected    = anz_selected - mu; 

M               = length(anz_selected); 

figure;
plot(t, anz_selected); 

%% compute auto correlation: Rx
[anz_ac, lags]   = xcorr(anz_selected, 'biased');   % 'biased'- scales the raw cross-correlation by 1/M

% display the correlation results
hAC     = figure(); 
plot(lags(M:2*M-1)*Ts, anz_ac(M:2*M-1)); % only plot positive side of autocorrelation
grid on; hold on; ylabel('Auto Correlation'); xlabel('time lag \tau (s)'); 

%% computer the power spectral density
sel_psd  = 1; % 1: using Welch spectral estimator; 2: using FFT

%% Welch's method 
if ( sel_psd == 1 )
    sp_welch    = spectrum.welch;                           % Create a Welch spectral estimator. 
    anz_psd0 = psd(sp_welch, anz_selected, 'Fs', Fs); % Calculate the PSD 

    % Plot the PSD
    hPSD       = figure(); 
    plot(anz_psd0); hold on;
    
    anz_psd  = anz_psd0.data; 
    
    % parameter estimation
    xdata = anz_psd0.frequencies; 
    ydata = anz_psd0.data; 
    
    param = lsqcurvefit( @(x,xdata) ( x(1)^2 )./ ( (2*pi*xdata).^2 ) + x(2)^2 ,[0.01, 0.1], xdata(2:129), ydata(2:129) );
    sigma_anz_bias      = abs( param(1) );
    sigma_anz           = abs( param(2) );
    
    % simulation
    freq        = xdata(2:end); 
    Sx_sim      =  ( sigma_anz_bias^2 ) ./ ( 2*pi*freq).^2  + sigma_anz^2 ;  
    
    figure(hPSD);
    plot(freq, 10*log10(Sx_sim), 'r'); grid on; axis tight; 
    legend('measured', 'estimated');
    title('Power Spectral Density'); 

    display(sigma_anz_bias);
    display(sigma_anz);

end

if ( sel_psd == 2 )
    % obtain nonparametric power spectral density (PSD) estimates
    % equivalent to the periodogram using fft
    N               = length(anz_selected);
    xdft            = fft(anz_selected);
    xdft            = xdft(1:N/2+1);
    psdx            = (1/(Fs*N)) * abs(xdft).^2;
    psdx(2:end-1)   = 2*psdx(2:end-1);
    freq            = 0:Fs/length(anz_selected):Fs/2;

    hPSD = figure();
    plot(freq,10*log10(psdx)); grid on; hold on; 
    title('Periodogram Using FFT')
    xlabel('Frequency (Hz)')
    ylabel('Power/Frequency (dB/Hz)'); 
    
    h_baro_psd      = psdx; 
    
end



