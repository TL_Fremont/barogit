function [x, K, P] = h_KF_baro_ins_4_state(x, u, z, P, Ts)

% use a third-order Kalman filters to replace a ninth-order Kalman filter 
% x = [x1, x2, x3] -------- states
%      x1 ----------------- INS position error (m)
%      x2 ----------------- INS velocity error (m/sec)
%      x3 ----------------- accelerometer bias (m/sec^2)
%      x4 ----------------- baro error (m)
% u = 0
% z = z_baro, error measurement for baro and laser
% Ts ---------------------- Sampling time


%% initialization
persistent A B C D H T Tao_acc Tao_bias Q R;
 
if isempty(A) 

    %% continous error model
    %Ts = 0.1;
    
    baro_sigma_c        = 0.8133;
    baro_tau            = 0.1384; 
    
    baro_sigma_square   = 2 * baro_sigma_c^2 / baro_tau; 
    
    a3 = -1/baro_tau; 
    
    A = [  0  1  0  0;
           0  0 -1  0;
           0  0  0  0;
           0  0  0 a3];
    
    B = [ 0; 1; 0; 0];
     
    C = [ 1  0  0 1];
      
    D =  0; 


    %% discrete plant model
    H = eye(4) + Ts * A + Ts^2/2 * A * A;
    T = [Ts*Ts/2; Ts; 0; 0]; 
    
%     Tao_acc  = 0.3^2;   % variance of acceleratmetor's gaussian noise
%     Tao_bias = 0.001^2; % variance of acceleratmetor bias's noise
    
    Tao_acc  = 0.0606^2;   % variance of acceleratmetor's gaussian noise
    Tao_bias = 0.0130^2; % variance of acceleratmetor bias's noise
  
    Q = [ Ts^3/3*Tao_acc+Ts^5/20*Tao_bias,  Ts^2/2*Tao_acc+Ts^4/8*Tao_bias,   -Ts^3/6*Tao_bias,       0;
          Ts^2/2*Tao_acc+Ts^4/8 *Tao_bias,      Ts*Tao_acc+Ts^3/3*Tao_bias,   -Ts^2/2*Tao_bias,       0;
                          -Ts^3/6*Tao_bias,                -Ts^2/2*Tao_bias,        Ts*Tao_bias,      0; 
                                         0,                               0,                  0,      baro_sigma_square * ( Ts + a3*Ts^2 + 2/3*a3^2*Ts^3 + 1/4*a3^3*Ts^4 + 1/20*a3^4*Ts^5)];
    
    R = (  7.7571e-04 * 10000 )^2; % Error measurements from baro

end


x_ = x;
u_ = u;
z_ = z; 
P_ = P; 

%% predict
x_ = H * x_ + T * u_;
P_ = H * P_ * H' + Q;

%% if the change of the velocity is too large, we do not update velocity
%if ( abs( z_(2) - x_(2) ) > 3 * P_(2,2) + 3 * sqrt( R(2,2) )  )
%    z_(2) = x_(2); 
%end

%% update
S = C * P_ * C' + R; 
K = P_ * C' / S;
x_ = x_ + K * (z_ - C * x_);
P_ = P_ - K * C * P_;


P = P_; 
x = x_;
