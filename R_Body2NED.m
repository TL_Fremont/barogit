% 
% Calculate the rotation matrix from body frame to NED frame
% 
function R = R_Body2NED(phi, tht, psi)

 R(1,1) = cos(psi)*cos(tht);
 R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
 R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
 R(2,1) = sin(psi)*cos(tht);
 R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
 R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
 R(3,1) = -sin(tht);
 R(3,2) = cos(tht)*sin(phi);
 R(3,3) = cos(tht)*cos(phi);