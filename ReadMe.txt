"analyze_pixhawk_baro.m": analyze the baro data and estimate the parameters of its error model;
"analyze_pixhawk_imu.m": analyze the imu data and estimate the parameters of its error model;
"Baro_INS_KF_Test.m": run the altitude estimation test using baro and ins;
"h_KF_baro_ins_4_state.m": Kalman filter for altitude estimation
